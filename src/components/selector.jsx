import React from 'react';

const Selector = ({ showSelector, setShowSelector, setCurrency, currency }) => {
    return (
        <div style={{ position: 'relative' }}>
            <button onClick={() => setShowSelector(!showSelector)} className='select-button'>{currency}</button>
            <div className={showSelector ? 'selector' : 'hidden'}>
                <button className='selector-options' onClick={() => {
                    setCurrency('USD');
                    setShowSelector(false);
                }}>USD</button>
                <button className='selector-options' onClick={() => {
                    setCurrency('EUR');
                    setShowSelector(false);
                }}>EUR</button>
                <button className='selector-options' onClick={() => {
                    setCurrency('UAH');
                    setShowSelector(false);
                }}>UAH</button>
            </div>
        </div>
    );
};

export default Selector;