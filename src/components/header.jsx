import React, { useEffect, useState } from 'react';

import axios from "axios";


const Header = () => {
    const [usdRate, setUsdRate] = useState();
    const [eurRate, setEurRate] = useState();

    // useEffect(() => {
    //     axios
    //         .get(`https://api.apilayer.com/fixer/convert?to=UAH&from=USD&amount=1`, {headers: {'apikey': 'MjEYsOvaTpqz4pLSSOXRF9aCozJqXYP0'}})
    //         .then(res => setUsdRate(res.data.result))
    //         .catch(err => console.log(err))
    //     axios
    //         .get(`https://api.apilayer.com/fixer/convert?to=UAH&from=EUR&amount=1`, {headers: {'apikey': 'MjEYsOvaTpqz4pLSSOXRF9aCozJqXYP0'}})
    //         .then(res => setEurRate(res.data.result))
    //         .catch(err => console.log(err))
    // }, []);

    return (
        <div className='header'>
            <h3>1 USD to UAH = {usdRate}</h3>
            <h3>1 EUR to UAH = {eurRate}</h3>
        </div>
    );
};

export default Header;