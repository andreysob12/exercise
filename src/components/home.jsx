import React, {useEffect, useState} from 'react';
import axios from "axios";

import Header from "./header";
import Selector from "./selector";

const Home = () => {
    const [firstInput, setFirstInput] = useState(1);
    const [secondInput, setSecondInput] = useState(0);

    const [firstCurrency, setFirstCurrency] = useState('UAH');
    const [secondCurrency, setSecondCurrency] = useState('USD');

    const [showFirstSelector, setShowFirstSelector] = useState(false);
    const [showSecondSelector, setShowSecondSelector] = useState(false);

    const [firstCurrencyRate, setFirstCurrencyRate] = useState(0);
    const [secondCurrencyRate, setSecondCurrencyRate] = useState(0);

    useEffect(() => {
        axios
            .get(`https://cdn.cur.su/api/latest.json`)
            .then(res => {
                setFirstCurrencyRate(res.data.rates[firstCurrency]);
                setSecondCurrencyRate(res.data.rates[secondCurrency]);
            })
    },[]);

    useEffect(() => {
        axios
            .get(`https://cdn.cur.su/api/latest.json`)
            .then(res => {
                setFirstCurrencyRate(res.data.rates[firstCurrency]);
                setSecondCurrencyRate(res.data.rates[secondCurrency]);
            })
    }, [firstCurrency, secondCurrency]);

    useEffect(() => {
        const currentCurrency = secondCurrencyRate / firstCurrencyRate;
        setSecondInput(firstInput * currentCurrency);
    }, [firstCurrencyRate, secondCurrencyRate]);

    function changeFirstInput(e) {
        setFirstInput(e.target.value);
        const currentCurrency = secondCurrencyRate / firstCurrencyRate;
        setSecondInput(e.target.value * currentCurrency);
    }

    function changeSecondInput(e) {
        setSecondInput(e.target.value);
        const currentCurrency = secondCurrencyRate / firstCurrencyRate;
        setFirstInput(e.target.value / currentCurrency);
    }

    return (
        <div className='home-page'>
            <div className='video-bg'>
                <video width="320" height="240" autoPlay loop muted>
                    <source src="https://assets.codepen.io/3364143/7btrrd.mp4" type="video/mp4" />
                </video>
            </div>
            <div className='window'>
                <div className='inside-window'>
                    <Header />
                    <div className='content'>
                        <div style={{ display: 'flex' }}>
                            <input className='amount-input' value={firstInput} onInput={changeFirstInput} type='number' />
                            <Selector showSelector={showFirstSelector} setShowSelector={setShowFirstSelector} currency={firstCurrency} setCurrency={setFirstCurrency} />
                        </div>
                        <p className='convert-in'>--></p>
                        <div style={{ display: 'flex' }}>
                            <input className='amount-input' value={secondInput} onInput={changeSecondInput} type='number' />
                            <Selector showSelector={showSecondSelector} setShowSelector={setShowSecondSelector} currency={secondCurrency} setCurrency={setSecondCurrency} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;